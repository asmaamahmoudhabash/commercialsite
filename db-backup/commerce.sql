-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2017 at 01:42 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `commerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(11, 'tablets', '2017-01-07 05:06:32', '2017-01-07 05:06:32'),
(12, 'laptops', '2017-01-07 05:06:42', '2017-01-07 05:06:42'),
(13, 'smartphones', '2017-01-07 05:06:56', '2017-01-07 05:06:56'),
(14, 'pc', '2017-01-07 05:07:09', '2017-01-07 05:07:09');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `describtion` varchar(255) NOT NULL,
  `price` decimal(55,0) NOT NULL DEFAULT '6',
  `image` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `describtion`, `price`, `image`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'infinx', 'Notice that the generator is smart enough to detect that you''re trying to create a table. When naming your migrations, m', '2', '/img/products/1483964138_1422296473-sony.jpg', 11, '2017-01-06 20:56:54', '2017-01-09 20:15:38'),
(2, 'macbook', 'macbookmacbookmacbookmacbookmacbook', '12', '/img/products/1483964423_1422296473-sony.jpg', 13, '2017-01-08 07:26:54', '2017-01-09 20:20:23'),
(5, 'apple', 'HP team announced to deprecate MySQL extension in 2011. Old MySQL extension officially deprecated since PHP 5.5.0 in late 2012 and it will be removed in the future. One of the alternatives announced to deprecate MySQL extension in 2011. ', '55', '/img/products/1483964594_computer_pc_PNG7722.png', 14, '2017-01-08 09:23:05', '2017-01-09 20:23:14'),
(7, 'coffe', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '12', '/img/products/1483964121_157d81f9-5e33-44b7-8c53-fdd4dc78ec83.jpg.w960.jpg', 11, '2017-01-08 11:00:20', '2017-01-09 20:15:22'),
(8, 'iphone', 'Git comes with built-in GUI tools for committing (git-gui) and browsing (gitk), but there are several third-party tools for users looking for p', '22', '/img/products/1483964176_1422471527-mac-book.jpg', 12, '2017-01-08 17:57:40', '2017-01-09 20:16:16'),
(9, 'hawiwi', 'Git comes with built-in GUI tools for committing ', '33', '/img/products/1483882170_1422296473-sony.jpg', 13, '2017-01-08 18:00:24', '2017-01-08 21:29:31'),
(10, 'Xiaomi', 'Nokia 6, Asus ZenFone AR, Other CES 2017 Launches, More News This Week ', '6666', '/img/products/1483963920_hqdefault.jpg', 11, '2017-01-09 20:12:01', '2017-01-09 20:12:01'),
(11, 'Moto', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new ', '55', '/img/products/1483964036_Samsung-GALAXY-Tab-7.0-Plus-1.jpg', 11, '2017-01-09 20:13:56', '2017-01-09 20:13:56'),
(12, 'Infocus', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '77', '/img/products/1483964264_1422471527-mac-book.jpg', 12, '2017-01-09 20:17:44', '2017-01-09 20:17:44'),
(13, 'LG', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '9999999', '/img/products/1483964321_1422471527-mac-book.jpg', 12, '2017-01-09 20:18:41', '2017-01-09 20:18:41'),
(14, 'sony', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '666', '/img/products/1483964372_1422471527-mac-book.jpg', 12, '2017-01-09 20:19:32', '2017-01-09 20:19:32'),
(15, 'nokia', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '555', '/img/products/1483965210_1422471527-mac-book.jpg', 13, '2017-01-09 20:20:47', '2017-01-09 20:33:30'),
(16, 'Xia', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '66', '/img/products/1483964474_1422296473-sony.jpg', 13, '2017-01-09 20:21:14', '2017-01-09 20:21:14'),
(17, 'computer', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '55', '/img/products/1483964572_computer_pc_PNG7722.png', 14, '2017-01-09 20:22:52', '2017-01-09 20:22:52'),
(18, 'com', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '55', '/img/products/1483964621_computer_pc_PNG7722.png', 14, '2017-01-09 20:23:41', '2017-01-09 20:23:41'),
(19, 'compu', 'Once installed, the simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, laravel new blog will create a directory named blog containing a fresh Laravel installation with all of Laravel''s dependenc', '1111', '/img/products/1483964652_computer_pc_PNG7722.png', 14, '2017-01-09 20:24:12', '2017-01-09 20:24:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`, `role`) VALUES
(6, 'amira', 'amira@amira', '$2y$10$CIq6bNJx9YXh9bos3anVDu3MR1ppQHvrakCxhp0HaHbJR/8DSRqYG', '2016-12-12 13:46:13', '2016-12-12 13:46:13', 1),
(7, 'mohamed', 'mohammed@mohammed', '$2y$10$.ri35sqanEApiH8URgYQDeeA4bjZ7RM32mBVf.lbw0bUnJyCui3i6', '2016-12-13 15:29:18', '2016-12-13 15:29:18', 0),
(8, 'ali', 'wema@gmail.com', '$2y$10$nJzihXUIX7YpODGUdL4sr.6DbV0KMutXMFUt.4bv0MzQWjsPcKPJa', '2016-12-13 16:28:35', '2016-12-13 16:28:35', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
