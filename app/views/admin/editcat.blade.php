@extends('layouts/main')
@section('title')
   Editing Category
@stop
@section('content')
    <h3>Editing Category:{{$category->name}}</h3>

    {{link_to_route('NewCategory','Add New Category')}}
    <hr>
    {{Form::model($category,array(
    'route'=>array(
    'updateCat',
    $category->id
    )))}}
    <p>{{Form::label('name')}}</p>
    <p>{{Form::text('name')}}</p>
    <p>{{$errors->first('name')}}</p>


    <p>{{Form::submit('update')}}</p>
    {{Form::close()}}


@stop
