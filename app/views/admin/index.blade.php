@extends('layouts/main')
@section('title')
    Admin Panal
    @stop
@section('content')
   <h3>Categories:</h3>

    @foreach($categories as $category)
        {{link_to_route('EditCategory',$category->name ,$category->id )}}||{{link_to_route('DeleteCategory','delete' ,$category->id )}}<br>
    @endforeach


   <h3>products:</h3>
   @foreach($products as $product)
       {{link_to_route('EditProduct',$product->name ,$product->id )}}||{{link_to_route('DeleteProduct','delete' ,$product->id )}}<br>
   @endforeach
@stop

@stop


