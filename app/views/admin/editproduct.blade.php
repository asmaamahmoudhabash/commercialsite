@extends('layouts/main')
@section('title')
   Edit Product
@stop
@section('content')
    <h3>Edit Product:</h3>


    <hr>
    {{Form::model($product,array(
    'route'=>array(
    'updateProduct',
    $product->id)
    ,'files'=>'true'
    ))}}
    <p>{{Form::label(' name','product name:')}}</p>
    <p>{{Form::text('name')}}</p>
    <p>{{$errors->first('name')}}</p>


    <p>{{Form::label('describtion','product Describtion:')}}</p>
    <p>{{Form::textarea('describtion')}}</p>
    <p>{{$errors->first('describtion')}}</p>


    <p>{{Form::label('price',' price:')}}</p>
    <p>{{Form::text('price')}}</p>
    <p>{{$errors->first('price')}}</p>


    <p>{{Form::label(' category','product category:')}}</p>
    <p>{{Form::select('category',$categories,$product->category_id)}}</p>
    <p>{{$errors->first('category')}}</p>

    <p>{{Form::label('image','product image:')}}</p>
    <p>{{Form::file('image')}} current image:{{HTML::image($product->image,$product->name,array('width'=>'100px'))}}  </p>
    <p>{{$errors->first('image')}}</p>


    <p>{{Form::submit('Add')}}</p>
    {{Form::close()}}


@stop
