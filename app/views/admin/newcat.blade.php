@extends('layouts/main')
@section('title')
    Add New Category
@stop
@section('content')
    <h3>Add New Category:</h3>

    {{link_to_route('NewCategory','Add New Category')}}
    <hr>
    {{Form::open(array('route'=>'storeCat'))}}
    <p>{{Form::label('name')}}</p>
    <p>{{Form::text('name')}}</p>
    <p>{{$errors->first('name')}}</p>


    <p>{{Form::submit('Add')}}</p>
    {{Form::close()}}


@stop
