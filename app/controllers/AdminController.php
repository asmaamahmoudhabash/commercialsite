<?php

class AdminController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function welcome()
	{
		//
		$categories=Category::all();
		$products=Product::all();
		return View::make('admin.index',compact('categories'))->with('products',$products);
	}


	public function newCategory()
	{
		//

		return View::make('admin.newcat');
	}





	public function store()
	{
		//
		$inputs = Input::all();
		$rules = array(

			'name' => 'required|min:2'
		);

		$vaildation=Validator::make($inputs,$rules);
		if($vaildation->fails()){
			return Redirect::back()->withInput()->withErrors($vaildation->messages());
		}
		$add=new Category();
		$add->name=Input::get('name');

		$add->save();
		return Redirect::route('Adminindex');



	}

	public function editCategory($id)
	{
		//
	  if(	$category=Category::find($id)){
		  return View::make('admin.editcat',compact('category'));
	  }else {

	  }

	}


	public function updateCategory($id)
	{
		//
		$inputs = Input::all();
		$rules = array(

			'name' => 'required|min:2'
		);

		$vaildation=Validator::make($inputs,$rules);
		if($vaildation->fails()){
			return Redirect::back()->withInput()->withErrors($vaildation->messages());
		}

		//dd(Input::all());
		$update=Category::find($id);
		$update->name=Input::get('name');

		$update->update();
		return Redirect::route('Adminindex');



	}

	public function deleteCategory($id)
	{
		$delete=Category::find($id);
		$delete->delete();
		return Redirect::route('Adminindex');

	}


   ##product

	public function newProduct()
	{
		//
        $categories=Category::lists('name','id');
		return View::make('admin.newproduct')->with('categories',$categories);
	}

	public function storeProduct()
	{
		//

		//dd(Input::all());

		$inputs = Input::all();
		$rules = array(

			'name' => 'required|min:2|alpha',
			'describtion'=>'required|min:3',
			'price'=>'required|numeric',
			'image'=>'required|image|mimes:png,jpeg,jpg',


		);

		$vaildation=Validator::make($inputs,$rules);
		if($vaildation->fails()){
			return Redirect::back()->withInput()->withErrors($vaildation->messages());
		}
		$add=new Product();
		$add->name=Input::get('name');
		$add->describtion=Input::get('describtion');
		$add->price=Input::get('price');
		$add->category_id=Input::get('category_id');
       ##uploding image before savimg it in DB
		
		$image=Input::file('image');
		$filename=time()."_".$image->getClientOriginalName();
		$path=public_path('/img/products/'.$filename);
		Image::make($image->getRealPath())->save($path);
		$add->image='/img/products/'.$filename;


		$add->save();
		return Redirect::route('Adminindex');



	}

	public function editProduct($id)
	{
		//
		if(	$product=Product::find($id)){
			$categories=Category::lists('name','id');

			return View::make('admin.editproduct',compact('product'))->with('categories',$categories);
		}else {

		}

	}


	public function updateProduct($id)
	{
		$inputs = Input::all();
		$rules = array(

			'name' => 'required|min:2|alpha',
			'describtion'=>'required|min:3',
			'price'=>'required|numeric',
			'image'=>'required|image|mimes:png,jpeg,jpg',


		);

		$vaildation=Validator::make($inputs,$rules);
		if($vaildation->fails()){
			return Redirect::back()->withInput()->withErrors($vaildation->messages());
		}
		$add=Product::find($id);
		$add->name=Input::get('name');
		$add->describtion=Input::get('describtion');
		$add->price=Input::get('price');
		$add->category_id=Input::get('category');
		##uploding image before savimg it in DB
  if(Input::hasFile('image'))##check if image has uploaded
		$image=Input::file('image');
		$filename=time()."_".$image->getClientOriginalName();
		$path=public_path('/img/products/'.$filename);
		Image::make($image->getRealPath())->save($path);

		File::delete('public/'.$add->image);

		$add->image='/img/products/'.$filename;


		$add->update();
		return Redirect::route('Adminindex');






	}

	public function deleteProduct($id)
	{
		$delete=Product::find($id);
		File::delete('public/'.$delete->image);
		$delete->delete();
		return Redirect::route('Adminindex');

	}

}