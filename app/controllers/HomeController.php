<?php

class HomeController extends BaseController
{

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}


	public function authenticate()
	{
		//dd(Input::all());
		$input = Input::all();
		$rules = array(

			'email' => 'required',
			'password' => 'required',


		);

		$vailidation = Validator::make($input, $rules);

		if ($vailidation->fails()) {
			return Redirect::back()->withInput()->withErrors($vailidation->messages());
		}

		$email = Input::get('email');
		$password = Input::get('password');
		if (Auth::attempt(array('email' => $email, 'password' => $password))) {
			$user = Auth::user();

			if ($user->role == 1) {
				return Redirect::route('Adminindex');
			} else {
				##go shopping
			}

		}


		Session::put('message', 'sorry, no user was found ');
		return Redirect::back()->withInput();

	}

	public function search()
	{
		#dd(Input::all());
		$products = Product::where('name', 'LIKE', '%' . Input::get('query') . '%')->orwhere('describtion', 'LIKE', '%' . Input::get('query') . '%')->get();
		$categories = Category::where('name', 'LIKE', '%' . Input::get('query') . '%')->get();
		return View::make('search', compact('categories'))->with('products', $products);

	}


	public function getCategory($id)
	{
		$categories = Category::all();
		$products = Product::where('category_id', '=', $id)->get();

		return View::make('category', compact('categories'))->with('products', $products)->withcurrent($id);

	}


	public function addToCart($id = 0, $qtt = 0)
	{

		if ($product = Product::find($id)) {
			$old_cart = [];
			$item = new Cart($id, $qtt);
			if (Session::has('cart')) {
				$old_cart = (array)Session::get('cart');

			}
			$old_cart[] = serialize($item);
			Session::put('cart', $old_cart);
			return json_encode('ok');
		} else {
			return json_encode('error');
		}


	}

	public function showCart()
	{


	}
}
