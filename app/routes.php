<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	$products=Product::paginate(4);
	$categories=Category::all();
	return View::make('hello')->with('products',$products)->with('categories',$categories);
});

Route::get('/login',function(){

	if(Auth::check()){
		return Redirect::to('/');
	}
	return View::make('login');

});

Route::post('/addToCart/{id}/{qtt}','HomeController@addToCart');

Route::get('/mycart','HomeController@showCart');
Route::get('/search',array('uses'=>'HomeController@search','as'=>'search'));
Route::post('/autenticate',array('uses'=>'HomeController@authenticate','as'=>'authenticate'));
Route::get('/category/{id}',array('uses'=>'HomeController@getCategory','as'=>'getCategory'));



    ##admin Routs
Route::group(array('prefix'=>'admin','before'=>'auth' ),function(){
     ##category Routs
	Route::get('/',array('uses'=>'AdminController@welcome','as'=>'Adminindex'));
	Route::get('newCategory',array('uses'=>'AdminController@newCategory','as'=>'NewCategory'));
	Route::get('editCategory/{id}',array('uses'=>'AdminController@editCategory','as'=>'EditCategory'));
	Route::get('deleteCategory/{id}',array('uses'=>'AdminController@deleteCategory','as'=>'DeleteCategory'));
	Route::post('storeCategory',array('uses'=>'AdminController@store','as'=>'storeCat'));
	Route::post('updateCategory/{id}',array('uses'=>'AdminController@updateCategory','as'=>'updateCat'));

	##Products Routs

	Route::get('newProduct',array('uses'=>'AdminController@newProduct','as'=>'NewProduct'));
	Route::get('editProduct/{id}',array('uses'=>'AdminController@editProduct','as'=>'EditProduct'));
	Route::get('deleteProduct/{id}',array('uses'=>'AdminController@deleteProduct','as'=>'DeleteProduct'));
	Route::post('storeProduct',array('uses'=>'AdminController@storeProduct','as'=>'storeProduct'));
	Route::post('updateProduct/{id}',array('uses'=>'AdminController@updateProduct','as'=>'updateProduct'));

});